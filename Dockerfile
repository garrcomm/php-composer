FROM php:7.3-apache

# Keep container up-to-date
RUN apt update
RUN apt upgrade -y

# Enable Apache modules
RUN a2enmod rewrite

# Send PHP errors to stderr so Docker will log this correctly
COPY docker-php-errorlogging.ini $PHP_INI_DIR/conf.d/docker-php-errorlogging.ini

# Install XDebug
COPY docker-php-ext-xdebug.ini $PHP_INI_DIR/conf.d/docker-php-ext-xdebug.ini
RUN pecl install xdebug
RUN docker-php-ext-enable xdebug

# Install MySQL client
RUN apt install mariadb-client-10.3 -y
RUN docker-php-ext-install mysqli

# Install composer
RUN apt install unzip git -y
RUN curl -sSL https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer

# Install the security checker
RUN curl -L https://github.com/fabpot/local-php-security-checker/releases/download/v1.0.0/local-php-security-checker_1.0.0_linux_amd64 --output local-php-security-checker
RUN mv local-php-security-checker /usr/local/bin/local-php-security-checker && chmod 755 /usr/local/bin/local-php-security-checker

# Install a few PHP more extensions
RUN apt-get install -y libxml2-dev zlib1g-dev libzip-dev
RUN docker-php-ext-install soap sockets zip && docker-php-ext-enable soap sockets zip


# Install Mailhog; many development environments use Mailhog to test with mails
RUN curl https://github.com/mailhog/mhsendmail/releases/download/v0.2.0/mhsendmail_linux_amd64 -L --output mhsendmail_linux_amd64
RUN chmod +x mhsendmail_linux_amd64
RUN mv mhsendmail_linux_amd64 /usr/local/bin/mhsendmail

# Enable SSL
RUN a2enmod ssl
RUN a2ensite default-ssl

# Install ssl-cert so we can self-sign a certificate
RUN apt install ssl-cert -y
RUN make-ssl-cert generate-default-snakeoil

# Fix permission issues
RUN groupmod -g 1001 www-data
RUN usermod -u 1000 www-data

# Cleanup
RUN rm -rf /var/lib/apt/lists/*

# Use wrapper as Docker command
COPY apache2-foreground-wrapper.sh /usr/local/bin/apache2-foreground-wrapper.sh
RUN chmod 755 /usr/local/bin/apache2-foreground-wrapper.sh
CMD ["/usr/local/bin/apache2-foreground-wrapper.sh"]
