#!/usr/bin/env sh

# Change Apache document root
if [ -n "$APACHE_DOCUMENT_ROOT" ]
then
  echo Configuring Apache Document Root to $APACHE_DOCUMENT_ROOT
  sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' $APACHE_CONFDIR/sites-available/*.conf
  sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' $APACHE_CONFDIR/apache2.conf $APACHE_CONFDIR/conf-available/*.conf
fi

if [ -n "$MAILHOG_ADDR" ]
then
  echo Configuring mhsendmail to $MAILHOG_ADDR
  echo "sendmail_path = \"/usr/local/bin/mhsendmail --smtp-addr=$MAILHOG_ADDR\"" > /usr/local/etc/php/conf.d/mailhog.ini
fi

# Run Apache in the foreground
/usr/local/bin/apache2-foreground
