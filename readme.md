# garrcomm/php73-composer

This Docker-container inherits the official Docker [php:7.3-apache](https://github.com/docker-library/php/blob/74175669f4162058e1fb0d2b0cf342e35f9c0804/7.3/buster/apache/Dockerfile) container,
with the following additions:

* X-Debug enabled and configured to connect to `host.docker.internal`
* MariaDB client 10.3
* [Composer 2.0](https://getcomposer.org/)
* [Local PHP Security Checker](https://github.com/fabpot/local-php-security-checker)
* [mhsendmail](https://github.com/mailhog/mhsendmail)
* Apache mod-ssl enabled with self-signed certificate
* A bunch of PHP extensions

## Environment values

This container supports the following environment values:

Value | Example | Description
---|---|---
APACHE_DOCUMENT_ROOT | /var/www/html/public | The path Apache must use as document root
MAILHOG_ADDR | mailhog:1025 | Host and port; when set, mails from PHP will be sent to a Mailhog container.
